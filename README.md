A simple password strength checker in JavaScript with jQuery (for convenience) and Bootstrap (for pretty).

You might want to replace the local copy of Bootstrap CSS with the CDN version.

An improvement would be to not count whitespace characters as special characters. 