
$( function() {

    $("#password_form")
        .on("keyup", "input", function(event) {
            testPasswords();
        })
        .on("change", "input", function(event) {
            testPasswords();
        });

    testPasswords();

});

/**
 * Test the password/confirmed password and show/hide indicators.
 */
function testPasswords() {
    var password = $("#password").val();
    var confirmed = $("#confirm").val();
    var $passwordIsGood = $("#passwordIsGood");
    var $confirmIsGood = $("#confirmIsGood");
    var $submit = $("#submit");

    checkPassword(password) ? $passwordIsGood.show() : $passwordIsGood.hide();
    if (checkPassword(confirmed) && confirmed === password) {
        $confirmIsGood.show();
        $submit.prop("disabled", false);
    } else {
        $confirmIsGood.hide();
        $submit.prop("disabled", true);
    }
}


/**
 * Check if the given string achieves minimum password strength requirements.
 *
 * @param word the password to check
 * @return boolean true if the password is good
 */
function checkPassword(word) {

    if (word === null || word.trim().length < 6) return false;  // too short
    if (!/[a-z]/.test(word)) return false;                      // no lowercase letter
    if (!/[A-Z]/.test(word)) return false;                      // no uppercase letter
    if (!/[0-9]/.test(word)) return false;                      // no number
    if (!/\W/.test(word)) return false;                         // no "special" character

    if (/([a-z]|[A-Z]){5,}/.test(word)) return false;           // word over 5 letters long
    if (/(.)\1{2,}/.test(word)) return false;                   // character repeated 3 times

    return true;

}